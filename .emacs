;; Place this file in C:/Users/{username}/AppData/Roaming and point to the appropriate files

;; Set this to the location where you have downloaded .emacs.d
(setq user-emacs-directory "D:/Personal/.emacs.d")
(setq user-init-file (concat user-emacs-directory "/init.el"))

;; Set this to where you have pulled the org directory.
(setq org-directory "D:/Personal/org")

;; Set this to your Documents folder (or any folder in which you'd like
;; emacs to start the C-x C-f find files commands)
;; For example:
;; "C:/Users/{username}/Documents/"
(setq default-directory "D:/")
(setenv "HOME" "D:/")
(load user-init-file)


