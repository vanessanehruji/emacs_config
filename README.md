# Getting Started with Emacs

This repository contains my personal configuration for Emacs and this README will teach you how to install and configure it with my settings.

## Getting Emacs
As of today, the latest stable Emacs is at version 27. This can be installed from the GNU website [here](https://www.gnu.org/software/emacs/download.html). Follow the link to download from a "nearby GNU mirror", navigate to the `emacs-27` folder at this mirror, and grab the `emacs-27.1-x86_64-installer.exe` file.

## Installing the Fonts
Download the two main fonts used in my configuration: 
1. DejaVu Sans Mono (from [here](https://www.fontsquirrel.com/fonts/dejavu-sans-mono))
2. Literata (from [here](https://fonts.google.com/specimen/Literata)). 

Unzip the files, open the ttf files within, and install the fonts. (You can check that they are installed by searching for them under the Fonts setting in Windows 10).

## Configuring Emacs
First clone this respository into a folder called `.emacs.d`. I normally install it in the root folder, i.e. in Windows, I would place it in `C:\Users\{username}`.

``` shell
git clone https://vanessanehruji@bitbucket.org/vanessanehruji/emacs_config.git .emacs.d
```

Then within the `.emacs.d` folder, open the `.emacs` file in a text editor (such as Notepad).
This file has the following contents

``` elisp
;; Place this file in C:/Users/{username}/AppData/Roaming and point to the appropriate files

;; Set this to the location where you have downloaded .emacs.d
(setq user-emacs-directory "D:/Personal/.emacs.d")
(setq user-init-file (concat user-emacs-directory "/init.el"))

;; Set this to your Documents folder (or any folder in which you'd like
;; emacs to start the C-x C-f find files commands)
;; For example:
;; "C:/Users/{username}/Documents/"
(setq default-directory "D:/")
(setenv "HOME" "D:/")
(load user-init-file)
```

Finally, to fix the fonts for the icons, simply type `M-x all-the-icons-install-fonts` in Emacs 


