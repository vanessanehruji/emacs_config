(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(use-package quelpa
  :ensure t
  :config
  (quelpa
   '(quelpa-use-package
     :fetcher git
     :url "https://github.com/quelpa/quelpa-use-package.git"))
  (require 'quelpa-use-package))

;;(setq debug-on-error t)

(setq inhibit-startup-message t
      inhibit-startup-echo-area-message t)

;;(global-linum-mode 1)
(require 'display-line-numbers)
(defcustom display-line-numbers-exempt-modes '(pdf-view-mode eshell-mode shell-mode org-mode)
  "Major modes on which to disable the linum mode, exempts them from global requirement"
  :group 'display-line-numbers
  :type 'list
  :version "green")

(defun display-line-numbers--turn-on ()
  "turn on line numbers but excempting certain majore modes defined in `display-line-numbers-exempt-modes'"
  (if (and
       (not (member major-mode display-line-numbers-exempt-modes))
       (not (minibufferp)))
      (display-line-numbers-mode)))

(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))

(add-hook 'org-mode-hook 'linum-mode)

(custom-set-faces
 '(line-number ((t (:family "DejaVu Sans Mono")))))

(setq display-line-numbers-width 4
      display-line-numbers-grow-only t)

(custom-theme-set-faces
 'user
 '(linum ((t (:inherit (shadow fixed-pitch) 
                       :background "#fbf8ef"
                       :foreground "#a8a8bf"
                       :strike-through nil
                       :underline nil
                       :slant italic
                       :weight normal
                       :height 120
                       :family "DejaVu Sans Mono")))))

(menu-bar-mode -1)

(scroll-bar-mode -1)

(tool-bar-mode -1)

;; (use-package smooth-scroll
;;     :ensure t)

(use-package hl-line
  :ensure t
  :hook
  (after-init . global-hl-line-mode))

(ido-mode t)

;;(desktop-save-mode 1)

(set-language-environment "UTF-8")
(prefer-coding-system       'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)

(customize-set-variable
 'backup-directory-alist
 `(("." . ,(concat user-emacs-directory "backups"))))

(setq version-control t     ;; Use version numbers for backups.
      kept-new-versions 10  ;; Number of newest versions to keep.
      kept-old-versions 0   ;; Number of oldest versions to keep.
      delete-old-versions t ;; Don't ask to delete excess backup versions.
      backup-by-copying t)  ;; Copy all files, don't rename them.

(setq create-lockfiles nil)

(setq visual-line-mode t)

(fset 'yes-or-no-p 'y-or-n-p)

(custom-theme-set-faces
 'user
 '(default ((t (:inherit (shadow fixed-pitch) :height 110 :family "DejaVu Sans Mono")))))

(setq-default line-spacing 0.3)

(defun set_transparency (val)
    (set-frame-parameter (selected-frame) 'alpha (cons val val))
    (add-to-list 'default-frame-alist `(alpha . ,(cons val val)))
)

(cond ((or (eq system-type 'darwin) (eq system-type 'windows-nt))
       (set_transparency 100))
      ((eq system-type 'gnu/linux)
       (set_transparency 90)))

;;  (set-frame-parameter (selected-frame) 'alpha '(90 . 90))
;;  (add-to-list 'default-frame-alist '(alpha . (90 . 90)))

(use-package centered-window
  :ensure t
  :config
  (setq cwm-centered-window-width 500)
  (setq cwm-frame-internal-border 60)
  (setq cwm-use-vertical-padding t)
  (centered-window-mode t))

(use-package golden-ratio-scroll-screen
  :ensure t
  :config
  (global-set-key [remap scroll-down-command] 'golden-ratio-scroll-screen-down)
  (global-set-key [remap scroll-up-command] 'golden-ratio-scroll-screen-up))

(use-package all-the-icons
  :ensure t)

;; (use-package doom-themes
;;   :ensure t
;;   :config
;;   ;; Global settings (defaults)
;;   (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
;;         doom-themes-enable-italic t) ; if nil, italics is universally disabled
;;   (load-theme 'doom-one t)

;;   ;; Enable flashing mode-line on errors
;;   (doom-themes-visual-bell-config)
;;   ;; Enable custom neotree theme (all-the-icons must be installed!)
;;   (doom-themes-neotree-config)
;;   ;; Corrects (and improves) org-mode's native fontification.
;;   (doom-themes-org-config))

(use-package doom-modeline
  :ensure t
  :config
  (setq doom-modeline-icon (display-graphic-p))
  :init
  (doom-modeline-mode 1))

(defun set-theme-dracula ()
  (load-theme 'dracula t)
  (custom-theme-set-faces
      'user
      '(linum ((t (:inherit (shadow fixed-pitch) 
                            :background "#282a36"
                            :foreground "#565761"
                            :strike-through nil
                            :underline nil
                            :slant italic
                            :weight normal
                            :height 120
                            :family "DejaVu Sans Mono"))))))

(use-package dracula-theme
  :ensure t
  :config
  ;; Don't change the font size for some headings and titles (default t)
  (setq dracula-enlarge-headings nil)

  ;; Adjust font size of titles level 1 (default 1.3)
  (setq dracula-height-title-1 1.25)

  ;; Adjust font size of titles level 2 (default 1.1)
  (setq dracula-height-title-1 1.15)

  ;; Adjust font size of titles level 3 (default 1.0)
  (setq dracula-height-title-1 1.05)

  ;; Adjust font size of document titles (default 1.44)
  (setq dracula-height-doc-title 1.4)

  ;; Use less pink and bold on the mode-line and minibuffer (default nil)
  (setq dracula-alternate-mode-line-and-minibuffer t)

  (set-theme-dracula))

(defun set-theme-spacemacs-light ()
  (load-theme 'spacemacs-light t)
  (custom-theme-set-faces
      'user
      '(linum ((t (:inherit (shadow fixed-pitch) 
                            :background "#fbf8ef"
                            :foreground "#a8a8bf"
                            :strike-through nil
                            :underline nil
                            :slant italic
                            :weight normal
                            :height 120
                            :family "DejaVu Sans Mono"))))))

(use-package spacemacs-theme
  :ensure t
  :defer t
  :init
  (set-theme-spacemacs-light))

(defvar *theme-light* 'spacemacs-light)
(defvar *theme-dark* 'dracula)
(defvar *current-theme* *theme-light*)

(defun toggle-theme ()
  (interactive)
  (if (eq (car custom-enabled-themes) 'dracula)
      (progn
        (disable-theme 'dracula)
        (set-theme-spacemacs-light))
    (progn
      (disable-theme 'spacemacs-light)
      (set-theme-dracula))))
(global-set-key [f5] 'toggle-theme)

(global-set-key (kbd "C-c k") 
                (lambda () (interactive) (find-file (concat user-emacs-directory "/init.org"))))

(global-set-key (kbd "C-c j") 
                (lambda () (interactive) (load-file (concat user-emacs-directory "/init.el"))))

(use-package company
  :ensure t
  :bind (:map company-active-map
              ("C-n" . company-select-next)
              ("C-p" . company-select-previous)
              ("<tab>" . company-complete-common-or-cycle)
         :map company-search-map
              ("C-p" . company-select-previous)
              ("C-n" . company-select-next))
  :init
  (setq company-backends '(company-capf
                           company-keywords
                           company-semantic
                           company-files
                           company-etags
                           company-elisp
                           company-clang
                           company-irony-c-headers
                           company-irony
                           company-jedi
                           company-cmake
                           company-ispell
                           company-yasnippet))

  :config
    (setq company-tooltip-maximum-width 60
          company-tooltip-minimum-width 60
          lsp-completion-provider :capf)
  :custom
  (company-tooltip-limit 5) 
  (company-idle-delay 0.15) 
  (company-minimum-prefix-length 3) ;; start company mode after 3 letters
  (company-selection-wrap-around t) 
  (company-require-match 'never) 
  :hook
  (after-init . global-company-mode)
  :custom-face
  (company-tooltip ((t (:family "DejaVuSansMono" :height 120)))))

;(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/helm")
(use-package helm
  :ensure t
  :bind
  (("C-x C-f" . helm-find-files)
   ("C-x b" . helm-mini)
   ("M-x" . helm-M-x)
   ("M-y" . helm-show-kill-ring))
  :config
  (require 'helm-config)
  (setq helm-split-window-in-side-p           t
        helm-move-to-line-cycle-in-source     t
        helm-ff-search-library-in-sexp        t
        helm-scroll-amount                    8
        helm-ff-file-name-history-use-recentf t
        helm-echo-input-in-header-line t)
  (helm-mode 1))

(use-package neotree
  :ensure t
  :config
  (global-set-key [f8] 'neotree-toggle)
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
  (setq neo-smart-open t)
  (setq neo-show-hidden-files t))

(use-package centaur-tabs
  :ensure t
  :config
  (setq centaur-tabs-set-bar 'over
        centaur-tabs-set-icons t
        centaur-tabs-grey-out-icons 'buffer
        centaur-tabs-height 34
        centaur-tabs-set-modified-marker t
        centaur-tabs-modified-marker "●")
  (centaur-tabs-change-fonts "DejaVu Sans Mono" 120)
  (centaur-tabs-group-by-projectile-project)
  (centaur-tabs-mode t))

(use-package dashboard
  :ensure t
  :config
    (dashboard-setup-startup-hook)
    (setq dashboard-items '((recents  . 5)
                            (projects . 5)))
    (setq dashboard-banner-logo-title ""))

(use-package projectile
  :ensure t
  :config
  (projectile-mode)
  :bind-keymap
  ("C-c p" . projectile-command-map))

(use-package helm-projectile
  :ensure t
  :config
  (helm-projectile-on))

(setq lsp-keymap-prefix "s-l")

(use-package lsp-mode
  :ensure t
  :hook (
         (python-mode . lsp)
         (c++-mode . lsp)
         (scala-mode . lsp)
         (lsp-mode . lsp-lens-mode)))

;; optionally
(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)

(use-package helm-lsp
  :ensure t
  :commands helm-lsp-workspace-symbol)

;Add metals backend for lsp-mode
(use-package lsp-metals)

(setq ccls-executable "/usr/bin/ccls")

(use-package lsp-pyright
  :ensure t
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp))))  ; or lsp-deferred

(use-package magit
  :ensure t
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package rainbow-delimiters
  :ensure t
  :hook
  (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode
  :ensure t
  :config
  (rainbow-turn-on))

(use-package org
     :ensure t
     :mode ("\\.org$" . org-mode)
     :bind
     (("C-c l" . org-store-link)
      ("C-c a" . org-agenda)
      ("C-c c" . org-capture))
     :hook ((org-mode . (lambda ()
                          (visual-line-mode)
                          (variable-pitch-mode)
                          (add-hook 'after-save-hook 'org-babel-tangle
                                    :append :local))))
     :config
     (setq org-startup-indented t)
     ;;(setq org-cycle-separator-lines 1)
     (setq org-hide-emphasis-markers t)
     (setq org-startup-folded nil)
     (setq org-log-done 'time)
     (setq org-todo-keywords '((type "TODO(t)" "NEXT(n)" "|" "DONE(d)" "CANCELLED(c@)")))
     (setq org-startup-with-inline-images t)
     (eval-after-load 'org
       (add-hook 'org-babel-after-execute-hook 'org-redisplay-inline-images))
     (setq org-cycle-include-plain-lists 'integrate)
     ;;(setq org-src-window-setup 'other-frame)
     (setq org-src-window-setup 'split-window-below)
     (custom-set-faces
      `(variable-pitch ((t (:family
                            ,(if (or (eq system-type 'darwin) (eq system-type 'gnu/linux))
                                 "Literata"
                               "Literata 12pt Medium")
                            :height 120)))))

     :custom-face
     ;;(variable-pitch ((t `(:family
     ;;                     ,(if (or (eq system-type 'darwin) (eq system-type 'gnu/linux))
     ;;                         "Literata"
     ;;                       "Literata 12pt Medium")
     ;;                     :height 120))))
     
     ;;(variable-pitch ((t (:family "Literata 12pt Medium" :height 120))))
     (fixed-pitch ((t (:family "DejaVu Sans Mono" :height 120))))
     (org-level-1 ((t (:inherit variable-pitch :height 160))))
     (org-level-2 ((t (:inherit variable-pitch :height 130))))
     (org-level-3 ((t (:inherit variable-pitch :height 130))))
     
     ;; Some settings with a different color scheme
     ;;(org-level-1 ((t (:foreground "#7cdedc"))))
     ;;(org-level-2 ((t (:foreground "#a9b3ce"))))
     ;;(org-level-3 ((t (:foreground "#9e788f"))))
     ;;(org-level-4 ((t (:foreground "#7284a8"))))
     ;; Set the font for the :PROPERTIES: drawer
     (org-drawer ((t (:inherit variable-pitch :height 90))))
     ;; These are for other tags in the drawer
     (org-special-keyword ((t (:inherit fixed-pitch :height 90)))) 
     (org-block ((t (:inherit fixed-pitch :height 115))))
     (org-code ((t (:inherit fixed-pitch :height 115))))
     (org-block-begin-line ((t (:inherit fixed-pitch :height 115))))
     (org-indent ((t (:inherit (org-hide fixed-pitch)))))
     (org-meta-line ((t (:inherit fixed-pitch :height 115))))
     (org-hide ((t (:inherit fixed-pitch :background "#fbf8ef" :foreground "#fbf8ef"))))
     (org-document-info-keyword ((t (:inherit fixed-pitch :height 115))))
     ;; Set a monospaced font for the table so that pipes align well.
     (org-table ((t (:inherit fixed-pitch))))
     (org-tag ((t (:background "#e8e3f0"))))
)

(defun set-window-padding ()
  (setq left-margin-width 5)
  (setq right-margin-width 5)
  (set-window-buffer nil (current-buffer)))

(use-package org-bullets
  :after org
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(use-package org-superstar
  :after org
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-superstar-mode 1))))

(use-package org-tempo
  :after org)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((C . t)
   (latex . t)
   (python . t)
   (js . t)
   (emacs-lisp . t)
   (lua . t)
   (shell . t)
   (R . t)
   (org . t)
   (ditaa . t)))

(setq org-ditaa-jar-path "/usr/share/java/ditaa/ditaa-0.11.jar")

(setq org-confirm-babel-evaluate nil)

(use-package org-projectile
  :ensure t
  :bind (("C-c n p" . org-projectile-project-todo-completing-read)
         ("C-c c" . org-capture))
  :config
  (progn
    (setq org-projectile-projects-file
          (concat user-org-directory "/notes/20201121090905-inbox.org"))
    (setq org-agenda-files (append org-agenda-files (org-projectile-todo-files)))
    (push (org-projectile-project-todo-entry) org-capture-templates)))

(use-package helm-org
  :ensure t)

(use-package org-ql
  :quelpa (org-ql :fetcher github :repo "alphapapa/org-ql"))

(use-package org-sidebar
    :quelpa (org-sidebar :fetcher github :repo "alphapapa/org-sidebar")
    :bind (("C-c s" . org-sidebar-toggle))
    :config
     (defun org-sidebar--todo-items (source-buffer)
       "Return an Org QL View buffer showing unscheduled, un-deadlined items in SOURCE-BUFFER."
       (let ((display-buffer
              (generate-new-buffer (format "Notes<%s>" (buffer-name source-buffer))))
             (title (propertize (concat "Notes in: " (buffer-name source-buffer))
                                'help-echo "Unscheduled, un-deadlined to-do items")))
         (with-current-buffer display-buffer
           (setf org-sidebar-source-buffer source-buffer))
         (save-window-excursion
           ;; `org-ql-search' displays the buffer, but we don't want to do that here.
           (org-ql-search source-buffer
             '(todo "NOTE")
             :buffer display-buffer
             :title title))
         display-buffer))
    (custom-set-variables
       '(org-sidebar-default-fns '(org-sidebar--todo-items)))
)

(use-package org-roam
      :ensure t
      :hook
      (after-init . org-roam-mode)
      :config
      (setq org-roam-title-sources '((title headline) alias))
      (setq org-roam-capture-templates
            '(("d" "default" plain (function org-roam-capture--get-point)
               "%?"
               :file-name "%<%Y%m%d%H%M%S>-${slug}"
               :head "#+title: ${title}\n#+created: %U"
               :unnarrowed t)))
      :custom
      (org-roam-directory (concat user-org-directory "/notes/"))
      :bind (:map org-roam-mode-map
              (("C-c n l" . org-roam)
               ("C-c n f" . org-roam-find-file)
               ("C-c n g" . org-roam-graph))
              :map org-mode-map
              (("C-c n i" . org-roam-insert))
              (("C-c n I" . org-roam-insert-immediate))))

(use-package deft
  :ensure t
  :bind ("<f9>" . deft)
  :commands (deft)
  :config (setq deft-directory "~/Personal/org/notes"
                deft-extensions '("org")
                deft-strip-summary-regexp (concat "\\("
                                                  "[\n\t]" ;; blank spaces
                                                  "\\|^#\\+[[:upper:][:lower:]_]+:.*$" ;; org-mode metadata
                                                  "\\|^- tags :: .*$" ;; tags
                                                  "\\|^- summary :: " ;; summary tag
                                                  "\\)")))

(global-set-key (kbd "C-c i") 
                (lambda () (interactive) (find-file "~/Personal/org/inbox.org")))

(setq org-agenda-files (list "~/Personal/org/inbox.org"
                             "~/Personal/org/constants.org"))

(setq org-capture-templates
      '(("i" "inbox" plain (file "~/Personal/org/inbox.org") "*** TODO %?")))

(defun add-property-with-date-captured ()
  "Add DATE_CAPTURED property to the current item."
  (interactive)
  (org-set-property "date-captured" (format-time-string "[%F %H:%M]")))

(add-hook 'org-capture-before-finalize-hook 'add-property-with-date-captured)

(setq org-agenda-custom-commands
      '(("d" "Dashboard"
         ((agenda "")
          (todo "NEXT"
                ((org-agenda-overriding-header "Next tasks:")))
          (todo "TODO"
                ((org-agenda-overriding-header "Unscheduled tasks:")))))))

;; Open agenda vertically by default
(defadvice org-agenda (around split-vertically activate)
  (let ((split-width-threshold 40)    ; or whatever width makes sense for you
        (split-height-threshold nil)) ; but never horizontally
    ad-do-it))

(setq org-agenda-window-setup 'reorganize-frame)
(setq org-agenda-restore-windows-after-quit t)
(setq org-agenda-window-frame-fractions '(0.5 . 0.5))

(use-package writeroom-mode
  :ensure t
  :hook ((org-mode . writeroom-mode)
         (org-mode . (lambda() (setq writeroom-width 100)))
         (prog-mode . writeroom-mode)
         (prog-mode . (lambda() (setq writeroom-width 120))))
  :config
  (custom-set-variables
       '(writeroom-global-effects
         '(writeroom-set-alpha
           writeroom-set-menu-bar-lines
           writeroom-set-tool-bar-lines
           writeroom-set-vertical-scroll-bars
           writeroom-set-bottom-divider-width)))
  (setq writeroom-mode-line t)
  (setq writeroom-maximize-window nil))

(use-package cmake-mode
  :ensure t)

(add-to-list 'auto-mode-alist '("\.cu$" . c++-mode))
(add-to-list 'auto-mode-alist '("\.cuh$" . c++-mode))

(use-package lua-mode
  :ensure t
  :mode (("\\.lua" . lua-mode)))

(autoload 'markdown-mode "markdown-mode"
  "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

(autoload 'gfm-mode "markdown-mode"
  "Major mode for GitHub Flavored Markdown files" t)
(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode))
  :init (setq markdown-command "/usr/bin/pandoc"))

(use-package rust-mode
  :ensure t
  :mode("\\.rs$" . rust-mode)
  :hook (rust-mode .
         (lambda () (setq indent-tabs-mode nil)))
  :config
  (setq rust-format-on-save t))

(use-package groovy-mode
  :ensure t
  :mode("Jenkinsfile" . groovy-mode))

(use-package scala-mode
  :interpreter
    ("scala" . scala-mode))

(use-package sbt-mode
  :ensure t
  :commands sbt-start sbt-command
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map)
   ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
   (setq sbt:program-options '("-Dsbt.supershell=false"))
)

(use-package tex-site
  :ensure auctex
  :mode ("\\.tex\\'" . latex-mode))

(use-package pdf-tools
  :ensure t
  :mode ("\\.pdf\\'" . pdf-tools-install)
  :bind ("C-c C-g" . pdf-sync-forward-search)
  :defer t
  :config
  (setq mouse-wheel-follow-mouse t)
  (setq pdf-view-resize-factor 1.10))

;; to use pdfview with auctex
(setq TeX-view-program-selection '((output-pdf "pdf-tools"))
      TeX-source-correlate-start-server t)
(setq TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view")))
;; to use pdfview with auctex
(add-hook 'LaTeX-mode-hook 'pdf-tools-install)

;;(add-hook 'prog-mode-hook 'linum-on)

;;(when (or (eq system-type 'darwin) (eq system-type 'gnu/linux))
;;         ((use-package vterm
;;            :ensure t)))
